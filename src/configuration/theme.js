import { teal, lightGreen, red, cyan } from "@material-ui/core/colors";

export default {
    palette: {
        common: {
            white: '#fff',
            black: '#292f36'
        },
        primary: {
            light: teal[100],
            main: teal[500],
            dark: teal[900]
        },
        secondary: {
            light: cyan[100],
            main: cyan[500],
            dark: cyan[900]
        },
        success: {
            light: lightGreen[100],
            main: lightGreen[500],
            dark: lightGreen[900]
        },
        danger: {
            light: red[100],
            main: red[500],
            dark: red[900]
        }
    }
}
