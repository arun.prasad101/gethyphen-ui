// react
import React from 'react';

// external libs
// mui
import AppBar from '@material-ui/core/AppBar';
import Avatar from '@material-ui/core/Avatar';
import Toolbar from '@material-ui/core/Toolbar';
import { makeStyles } from '@material-ui/core/styles';

// components
// services
// errors
// utils
// configuration
// assets
import logo from '../../assets/logo.png';

// styles
const useStyles = makeStyles( theme => ({
    appBar: {
        height: 64,
        background: '#2F2F2F',
        color: theme.palette.common.white,
        zIndex: theme.zIndex.drawer + 1
    },

    header: {
        width: '100%',
        justifyContent: 'space-between'
    },

    logo: {
        height: 40,
        marginRight: 20
    },

    avatar: {
        backgroundColor: theme.palette.primary.main,
        color: theme.palette.common.white,
        marginRight: 10,
        fontSize: 14,
        height: 30,
        width: 30
    }
}));

export default function Header() {
    const classes = useStyles();

    return (
        <AppBar position='fixed' className={classes.appBar}>
            <Toolbar className={classes.header}>
                <img src={logo} alt='Hyphen' className={classes.logo} />
                <Avatar className={classes.avatar}>
                    AP
                </Avatar>
            </Toolbar>
        </AppBar>
    );
}
