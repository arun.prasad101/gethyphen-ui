// react
import React from 'react';

// ext libs
// mui
import { makeStyles } from '@material-ui/core/styles';

// components
import Header from './Header';

// hooks
// services
// utils
// configuration
// assets
// styles
const useStyles = makeStyles({
    appRoot: {
        display: 'flex',
        height: '100%',
        maxHeight: '100%'
    },
    pageContentRoot: {
        padding: '64px 20px 40px',
        flexGrow: 1
    }
});

export default function Layout({ children }) {

    const classes = useStyles();

    return (
        <main className={classes.appRoot}>
            <Header />
            <section className={classes.pageContentRoot}>
                {children}
            </section>
        </main>
    );
}
