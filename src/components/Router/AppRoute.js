// react
import React from 'react';

// ext libs
import { Route } from 'react-router-dom';

// mui
// components
import Layout from '../Layout';


export default function AppRoute({ component: Component, ...rest }) {
    return (
        <Route
            {...rest}
            render={ props => <Layout>
                <Component {...props} />
            </Layout>}
        />
    )
}
