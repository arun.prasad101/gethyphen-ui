// react
import React from 'react';
import PropTypes from 'prop-types';

// ext libs
// mui
import Typography from '@material-ui/core/Typography';

import { makeStyles } from '@material-ui/core/styles';

// components
// hooks
// services
// utils
// configuration
// assets
// styles
const useStyles = makeStyles({
    root: {
        display: 'flex',
        flex: 1,
        justifyContent: 'space-between',
        marginTop: 20,
        marginBottom: 40,
        padding: '10px 24px',
        borderBottom: '1px solid #ccc'
    },
    title: {
        fontWeight: 300
    }
});

export default function ActionBar({ title, actions }) {

    const classes = useStyles();

    return (
        <section className={classes.root}>
            <Typography variant='h3' className={classes.title}>
                {title}
            </Typography>
            {actions}
        </section>
    );
}

ActionBar.propTypes = {
    title: PropTypes.string,
    actions: PropTypes.node
};

