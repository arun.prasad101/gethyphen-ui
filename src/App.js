// react
import React from 'react';

// ext libs
import { BrowserRouter } from 'react-router-dom';
import { SnackbarProvider } from 'notistack';

// mui
import { MuiThemeProvider, createMuiTheme } from '@material-ui/core/styles';
import CssBaseline from '@material-ui/core/CssBaseline';

// views
import Reporting from './views/Reporting';

// components
import AppRoute from './components/Router/AppRoute';

// hooks
// services
// assets
// styles
import theme from './configuration/theme';
const muiTheme = createMuiTheme(theme);

export default function App() {
    return (
        <SnackbarProvider maxSnack={3}>
            <MuiThemeProvider theme={muiTheme}>
                <CssBaseline />
                <BrowserRouter>
                    <AppRoute path='/' component={Reporting} />
                </BrowserRouter>
            </MuiThemeProvider>
        </SnackbarProvider>
    );
}
