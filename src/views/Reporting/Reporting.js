// react
import React from 'react';

// ext libs
import { useSnackbar } from 'notistack';

// mui
import { makeStyles } from '@material-ui/core/styles';
import CircularProgress from '@material-ui/core/CircularProgress';
import FormControl from '@material-ui/core/FormControl';
import InputLabel from '@material-ui/core/InputLabel';
import MenuItem from '@material-ui/core/MenuItem';
import Select from '@material-ui/core/Select';
import Table from '@material-ui/core/Table';
import TableBody from '@material-ui/core/TableBody';
import TableCell from '@material-ui/core/TableCell';
import TableHead from '@material-ui/core/TableHead';
import TableRow from '@material-ui/core/TableRow';
import Typography from '@material-ui/core/Typography';

// components
import ActionBar from '../../components/ActionBar';

// hooks
// services
// utils
// configuration
import { BASE_URL } from '../../configuration/app';

// assets
// styles
const useStyles = makeStyles(theme => ({
    root: {
        display: 'flex',
        flexDirection: 'column',
        flex: 1
    },
    tableHead: {
        color: theme.palette.primary.main
    },
    totalSentimentContainer: {
        marginTop: 30,
    },
    totalSentiment: {
        paddingLeft: 20,
        fontWeight: 500,
        color: theme.palette.info.main
    }
}));

export default function Reporting() {

    const classes = useStyles();
    const { enqueueSnackbar } = useSnackbar();

    const [ dimension, setDimension ] = React.useState('location');
    const [ isLoading, setIsLoading ] = React.useState(true);
    const [ data, setData ] = React.useState({
        sentimentByDimension: [],
        totalSentiment: 0
    });

    React.useEffect(() => {
        function _onSuccess(data) {
            if (data.error) {
                enqueueSnackbar('Oops! Unable to get data, please try again');
            } else {
                setData({
                    sentimentByDimension: data.sentimentByDimension,
                    totalSentiment: data.totalSentiment
                });
            }
        }

        function _getSentimentByDimension() {
            setIsLoading(true);
            fetch(BASE_URL + 'sentiment/' + dimension, {
                mode: 'cors',
                cache: 'no-cache',
                headers: { 'Content-Type': 'application/json' },
                credentials: 'same-origin'
            })
                .then((response) => response.json())
                .then(_onSuccess)
                .finally(() => setIsLoading(false))
        }

        _getSentimentByDimension();
    }, [dimension]);

    function onChangeDimension(event) {
        const { value } = event.target;
        setDimension(value);
    }

    return (
        <div className={classes.root}>
            <ActionBar
                title='Sentiment Report'
                actions={
                    <DimensionFilter
                        selectedDimension={dimension}
                        onChange={onChangeDimension}
                    />
                }
            />
            {isLoading && <CircularProgress color='secondary' />}
            {!isLoading && data.sentimentByDimension.length > 0 &&
                <SentimentByDimensionTable data={data.sentimentByDimension} />
            }
            {!isLoading && data.sentimentByDimension.length > 0 &&
                <div className={classes.totalSentimentContainer}>
                    <Typography variant='h5'>Total Sentiment
                        <span className={classes.totalSentiment}>{data.totalSentiment}</span>
                    </Typography>
                </div>
            }
            {!isLoading && data.sentimentByDimension.length === 0 &&
                <Typography>No data found :(</Typography>
            }
        </div>
    );
}

function SentimentByDimensionTable({ data }) {

    const classes = useStyles();

    return (
        <Table>
            <TableHead>
                <TableRow>
                    <TableCell className={classes.tableHead}>Segment</TableCell>
                    <TableCell className={classes.tableHead} align='right'>
                        Sentiment Score
                    </TableCell>
                    <TableCell className={classes.tableHead}align='right'>
                        Participation %
                    </TableCell>
                </TableRow>
            </TableHead>
            <TableBody>
                {data.map((row, index) => (
                    <TableRow key={index}>
                        <TableCell component='th' scope='row'>
                            {row.dimension}
                        </TableCell>
                        <TableCell align='right'>{row.sentiment}</TableCell>
                        <TableCell align='right'>{row.participationPercentage}</TableCell>
                    </TableRow>
                ))}
            </TableBody>
        </Table>
    );
}

function DimensionFilter({ selectedDimension, onChange }) {

    const classes = useStyles();

    return (
        <FormControl variant='standard' className={classes.formControl}>
            <Select
                id='dimension'
                value={selectedDimension}
                onChange={onChange}
            >
                <MenuItem value={'location'}>Location</MenuItem>
                <MenuItem value={'department'}>Department</MenuItem>
                <MenuItem value={'designation'}>Designation</MenuItem>
            </Select>
        </FormControl>
    );
}
