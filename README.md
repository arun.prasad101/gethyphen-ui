# UI

## Steps to run
1. Clone the repo
2. npm install
3. npm start

## Completed
- UI to display the sentiment data filtered by selected segment.

## Incomplete
- Test cases
- Security (XSS filtering & CSRF)
- Session management

## Known Issues